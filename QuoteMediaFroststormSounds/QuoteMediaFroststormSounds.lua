local moduleName = "QuoteMediaFroststormSounds";
local moduleid = "Fro";
local moduleData = nil;
local frame = CreateFrame("Frame", ""..moduleName.."FakeFrame");

function QuoteMediaFroststormSounds_OnEvent(this, event, arg1, arg2, arg3, arg4, ...)
    if ((event == "ADDON_LOADED") and (arg1 == moduleName)) then

        if (IsAddOnLoaded("SoundDelivery") or IsAddOnLoaded("WoWQuote2") or IsAddOnLoaded("KQuote")) then
            QuoteMediaFroststormSounds_Init();
            if (IsAddOnLoaded("SoundDelivery")) then
                SD_InitModule(moduleData);
            end
            if (IsAddOnLoaded("WoWQuote2")) then
                WQ2_InitModule(moduleData);
            end
            if (IsAddOnLoaded("KQuote")) then
                KQ_InitModule(moduleData);
            end            
        else
            DEFAULT_CHAT_FRAME:AddMessage(moduleName..": Please install KQuote, WoWQuote2, SoundDelivery!",1.0,0.0,0.0);
        end
        frame:UnregisterEvent("ADDON_LOADED");
    end
end

frame:SetScript("OnEvent", QuoteMediaFroststormSounds_OnEvent);
frame:RegisterEvent("ADDON_LOADED");

function QuoteMediaFroststormSounds_Init()
    moduleData = {
        name = "QuoteMediaFroststormSounds",
        moduleid = moduleid,
        mediapath = "Interface\\AddOns\\"..moduleName.."\\media\\",
        mediadata = nil
    }

    moduleData.mediadata = {
        {
            id = "Fro:001",
            file = "Das war ja riesig.mp3",
            len = 12,
            msg = "Das war ja riesig",
            tag = "Sound",
        },
        {
            id = "Fro:002",
            file = "Du hast garkeine Freunde.mp3",
            len = 5,
            msg = "Du hast garkeine Freunde",
            tag = "Sound",
        },
        {
            id = "Fro:003",
            file = "Eat shit and die.mp3",
            len = 8,
            msg = "Eat shit and die",
            tag = "Sound",
        },
        {
            id = "Fro:004",
            file = "Ein Fisch.mp3",
            len = 14,
            msg = "Ein Fisch",
            tag = "Sound",
        },
        {
            id = "Fro:005",
            file = "Good morning.mp3",
            len = 2,
            msg = "Good morning",
            tag = "Sound",
        },
        {
            id = "Fro:006",
            file = "halt stopp das bleibt alles so wies hier ist.mp3",
            len = 6,
            msg = "halt stopp das bleibt alles so wies hier ist",
            tag = "Sound",
        },
        {
            id = "Fro:007",
            file = "Ich bin Feuer ich bin der Tod.mp3",
            len = 16,
            msg = "Ich bin Feuer ich bin der Tod",
            tag = "Sound",
        },
        {
            id = "Fro:008",
            file = "Ich Frage mich wo dieser Fisch hingegangen ist.mp3",
            len = 9,
            msg = "Ich Frage mich wo dieser Fisch hingegangen ist",
            tag = "Sound",
        },
        {
            id = "Fro:009",
            file = "Immer fort.mp3",
            len = 5,
            msg = "Immer fort",
            tag = "Sound",
        },
        {
            id = "Fro:010",
            file = "Seht doch mal im Ruessel nach.mp3",
            len = 3,
            msg = "Seht doch mal im Ruessel nach",
            tag = "Sound",
        },
        {
            id = "Fro:011",
            file = "Thriller laugh.mp3",
            len = 12,
            msg = "Thriller laugh",
            tag = "Sound",
        },
        {
            id = "Fro:012",
            file = "Was das denn.mp3",
            len = 2,
            msg = "Was das denn",
            tag = "Sound",
        },
        {
            id = "Fro:013",
            file = "Was sind das alles fuer Schluessel hier.mp3",
            len = 6,
            msg = "Was sind das alles fuer Schluessel hier",
            tag = "Sound",
        },
        {
            id = "Fro:014",
            file = "Wir haben ueberlebt nur wegen mir.mp3",
            len = 3,
            msg = "Wir haben ueberlebt nur wegen mir",
            tag = "Sound",
        },
        {
            id = "Fro:015",
            file = "Wir muessen ihn haben den Schatz.mp3",
            len = 11,
            msg = "Wir muessen ihn haben den Schatz",
            tag = "Sound",
        },

    };
end
